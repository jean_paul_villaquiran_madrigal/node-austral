require('newrelic');
require('dotenv').config();
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const passport = require('./config/passport');
const session = require('express-session');
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var bicicletasRouter = require('./routes/bicicletas');
var bicicletasAPIRouter = require('./routes/api/bicicletas');
var usuariosAPIRouter = require('./routes/api/usuarios');
var emailAPIRouter = require('./routes/api/email');
var tokenRouter = require('./routes/token');
var usuariosRouter = require('./routes/usuarios');
var loginRouter = require('./routes/login');
var sessionRouter = require('./routes/session');
const jwt = require('jsonwebtoken');
var apiAuthRouter = require('./routes/api/auth');
const MongoDBStore = require('connect-mongodb-session')(session);

let store;
if(process.env.NODE_ENV === 'development'){
  store = new session.MemoryStore;
} else {
  store = new MongoDBStore({
    uri: process.env.MONGO_URI,
    collection: 'sessions'
  });

  store.on('error', function(error){
    assert.ifError(error);
    assert.ok(false);
  });
}

var app = express();

app.set('secretKey','jwt_pwd:123456789');

app.use(session({
  cookie: { maxAge: 240 * 60 * 60 * 1000 },
  store: store,
  saveUninitialized: true,
  resave: 'true',
  secret: 'red_bicis_!!!!'
}));

var mongoose = require('mongoose');
const { assert } = require('console');
var mongoDB = process.env.MONGO_URI;
mongoose.connect(mongoDB, { useNewUrlParser: true });
mongoose.set('useFindAndModify', false);
mongoose.Promise = global.Promise;

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error: '));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/token', tokenRouter);
app.use('/usuarios', usuariosRouter);
app.use('/email', emailAPIRouter);
app.use('/users', usersRouter);
app.use('/bicicletas', loggedIn , bicicletasRouter);
app.use('/api/bicicletas', validarUsuario, bicicletasAPIRouter);
app.use('/api/usuarios', usuariosAPIRouter);
app.use('/email', emailAPIRouter);
app.use('/login', loginRouter);
app.use('/', sessionRouter);
app.use('/api/auth', apiAuthRouter);

app.use('/privacy_policy', function(req, res){
  res.sendFile('public/privacy_policy.html');
});

app.use('/googlece501d549f4d33bf', function(req, res){
  res.sendFile('public/googlece501d549f4d33bf.html');
});

app.get('/auth/google',    
    passport.authenticate('google', { scope: [
      'profile',
      'email']}));

app.get('/auth/google/callback', passport.authenticate('google', {
    failureRedirect: '/error',
    successRedirect: '/'}));


app.get('/logout', (req, res) => {
  req.logout();
  res.redirect('/');
});

function loggedIn(req, res, next) {
  if (req.user) {
    next();
  } else {
    console.log('user sin loguearse');
    res.redirect('/login');
  }
}

function validarUsuario(req, res, next){
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function(err, decode){
    if(err){
      res.json({status:'Error', message: err.message, data:null});
    } else {
      req.body.userId = decode.id;
      console.log('jwt verify: '+ decode);
      next();
    }
  });
}

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;