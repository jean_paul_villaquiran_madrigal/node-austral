const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const usuario = require('../models/usuario');
const GoogleEstrategy = require('passport-google-oauth20').Strategy;
const FacebookTokenEstrategy = require('passport-facebook-token');

passport.use(new localStrategy(
    function (email, password, done) {
        usuario.findOne({ email: email }, function (err, usuario) {
            if (err) {
                return done(err);
            }

            if (!usuario) {
                return done(null, false, { message: 'Email no existente o incorrecto.' });
            }

            if (!usuario.validPassword(password)) {
                return done(null, false, { message: 'Password incorrecto' });
            }

            return done(null, usuario);
        });
    }
));

passport.use(new GoogleEstrategy({
    clientID: process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    callbackURL: process.env.HOST + "/auth/google/callback"
},
    function (accessToken, refreshToken, profile, cb) {
        console.log('Profile: ' + profile);
        usuario.findOneOrCreateByGoogle(profile, function (err, user) {
            return cb(err, user);
        });
    }
));

passport.use(new FacebookTokenEstrategy({
    clientID: process.env.FACEBOOK_ID,
    clientSecret: process.env.FACEBOOK_SECRET
},
    function (accessToken, refreshToken, profile, done) {
        try {
            console.log('Profile: ' + profile);
            usuario.findOneOrCreateByFacebook(profile, function (err, user) {
                if (err) {
                    console.log('err: ' + err);
                }
                return done(err, user);
            });
        } catch (error) {
            console.log(error);
            return done(err, user);
        }
    }
));

passport.serializeUser((user, cb) => {
    cb(null, user.id);
});

passport.deserializeUser((id, cb) => {
    usuario.findById(id, (err, usuario) => {
        cb(err, usuario);
    });
});

module.exports = passport;