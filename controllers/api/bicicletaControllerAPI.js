var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(request, response) {
    
    var query = Bicicleta.find({});
               
    query.exec((error, bicicletas) => {
        
        if(error){
            return response.status(500).send({
                status: 'error',
                bicicletas: 'Error al obtener los articulos'
            });
        }

        if(!bicicletas){
            return response.status(404).send({
                status: 'error',
                bicicletas: 'Error al obtener los articulos'
            });
        }

        return response.status(200).send({
            status: 'success',
            bicicletas: bicicletas
        });
    });
}

exports.bicicleta_create = function(request, response){
    var bici = new Bicicleta();
    bici.code = request.body.code;
    bici.color = request.body.color;
    bici.modelo = request.body.modelo;
    bici.ubicacion = [request.body.lat, request.body.lng];
    console.log('create color: '+request.body.color);
    Bicicleta.agregar(bici);
    response.status(200).json({
        bicicleta: bici
    })
}

exports.bicicleta_delete = function(request, response){

    Bicicleta.remove(request.body.id);
    response.status(200).json({
        status: 'Success',
        message: 'Bicicleta eliminada correctamente'
    })
}

exports.bicicleta_delete_many = function(request, response){

    Bicicleta.deleteMany({}, (error, success) => {
        response.status(200).json({
            status: 'Success',
            message: 'Todas las bicicletas eliminadas correctamente'
        })
    });
}

exports.bicicleta_update = function(request, response){
    console.log('api update: '+request.body.id);
    var bicicleta = Bicicleta.findById(request.body.id);
    console.log('fin findById api update: '+request.body.id);
    bicicleta.id = request.body.id;
    bicicleta.color = request.body.color;
    bicicleta.modelo = request.body.modelo;
    bicicleta.ubicacion = [request.body.lat, request.body.lng];

    response.status(200).json({
        status: 'Success',
        message: 'Bicicleta actualizada correctamente'
    })
}