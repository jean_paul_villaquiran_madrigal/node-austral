var nodemailer = require('nodemailer');

exports.sendEmail = function(req, res){
    // Definimos el transporter
    console.log("request:"+req);
    console.log("repsonse:"+res);
    const transporter = nodemailer.createTransport({
        host: 'smtp.ethereal.email',
        port: 587,
        secure: false,
        auth: {
            user: 'gabriella.hills@ethereal.email',
            pass: 'YjSbpShh3UnkeaRKWj'
        },
        tls: {
            rejectUnauthorized: false
        }
    });

    // Definimos el email
    
    var mailOptions = {
        from: req.from,
        to: req.to,
        subject: req.subject,
        text: req.text
    };

    // Enviamos el email
    transporter.sendMail(mailOptions, function(error, info){
        if (error){
            console.log(error);
            res.send(500, error.message);
        } else {
            console.log("Email sent");
            res.status(200).jsonp(req.body);
        }
    });
};