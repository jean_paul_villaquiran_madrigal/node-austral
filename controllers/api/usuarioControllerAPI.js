var Usuario = require('../../models/usuario');

exports.usuario_list = function(request, response) {
    
    var query = Usuario.find({});
               
    query.exec((error, usuarios) => {
        
        if(error){
            return response.status(500).send({
                status: 'error',
                usuarios: 'Error al obtener los usuarios'
            });
        }

        if(!usuarios){
            return response.status(404).send({
                status: 'error',
                usuarios: 'Error al obtener los usuarios'
            });
        }

        return response.status(200).send({
            status: 'success',
            usuarios: usuarios
        });
    });
}

exports.usuario_create = function(request, response){
    var usuario = new Usuario();
    usuario.nombre = request.body.nombre;    
    console.log('create usuario: '+request.body.nombre);
    usuario.save((err, usuarioStored) => {

        if(err){
            console.error(err);

            return response.status(404).json({
                status: 'error',
                message: 'El usuario no fue guardado'
            });
        }

        if(response){
            return response.status(200).json({
                status: 'success',
                usuario: usuarioStored
            });
        }
    });
    
}