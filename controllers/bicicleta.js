var Bicicleta = require('../models/bicicleta');

exports.bicicleta_list = function(request, responde){ 
    Bicicleta.allBicis((err, bicicletas) => {
        console.log("allBicis: "+bicicletas);

        if(err){
            console.log('Bicicleta no encontrada');
            return next();
        }

        responde.render('bicicletas/index',{bicis: bicicletas});
    });
}

exports.bicicleta_create_get = function(request, response){
    response.render('bicicletas/create');
}

exports.bicicleta_create_post = function(req,res,next){
    Bicicleta.create({
        code: req.body.code,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: [req.body.lat, req.body.lng],
    }, function(err) {
        if (err) {
            res.render('bicicletas/create', {
                errors: err.errors,
                bicicleta: new Bicicleta({
                    code: req.body.code,
                    color: req.body.color,
                    modelo: req.body.modelo,
                    ubicacion: [req.body.lat, req.body.lng],
                })
            });
        } else {
            res.redirect('/bicicletas');
        }
    });
}

exports.bicicleta_update_get = function(request, response){
    var bicicleta = Bicicleta.findById(request.params.id);
    response.render('bicicletas/update', {bicicleta});
}

exports.bicicleta_update_post = function(request, response){
    var bicicleta = Bicicleta.findById(request.body.id);
    console.log('Bicicleta encontrasa: '+bicicleta.modelo);
    bicicleta.id = request.body.id;
    bicicleta.color = request.body.color;
    bicicleta.modelo = request.body.modelo;
    bicicleta.ubicacion = [request.body.lat, request.body.lng];

    response.redirect('/bicicletas');
}

exports.bicicleta_remove = function(request, response, next) {
    Bicicleta.findByIdAndDelete(request.body.id, (err) => {
        if(err){            
            next(err);
        } else {
            response.redirect('/bicicletas');
        }
    });
    
}