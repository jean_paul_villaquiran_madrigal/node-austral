var Usuario = require('../models/usuario');
const passport = require('passport');
const usuario = require('../models/usuario');
const express = require('express');
const { use } = require('passport');

module.exports = {
    login_get: (req, res, next) => {
        console.log('Login get');
        res.render('session/login', {errors:{}, message: {}, usuario: new Usuario()}); 
    },

    login: (req, res, next) => {
        passport.authenticate('local',(err, user, info) => {
            if(err){
                return next(err);
            } 
            if(!user){
                return res.render('session/login', {info});
            }

            req.logIn(user, function(err) {
                console.log(user);
                if (err) return next(err);
                return res.redirect('/');
              });
        })(req, res, next);  
    },

}