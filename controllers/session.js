const Usuario = require("../models/usuario");
const Token = require("../models/token");

module.exports = {
    forgotPassword_get: function (req, res) {
        res.render('session/forgotPassword');
    },

    forgotPassword: function (req, res) {
        Usuario.findOne({ email: req.body.email }, function (err, usuario) {
            if (!usuario) {
                return res.render('session/forgotPassword', { info: { message: 'No existe el email para un usuario existente.' } });
            }

            usuario.resetPassword(function (err) {
                if (err) {
                    return next(err);
                }

                console.log('session/forgotPassword');
            });

            res.render('session/forgotPasswordMessage');
        });
    },

    resetPassword_get: function (req, res, next) {
        console.log('parametro: '+req.params.token);
        Token.findOne({ token: req.params.token }, function (err, token) {
            if (!token) {
                return res.status(400).send({ msg: 'No existe un usuario asociado al token, verifique que su token no haya expirado' });
            }

            Usuario.findById(token._userId, function (err, usuario) {
                if (!usuario) return res.status(400).send({ msg: 'No existe un usuario asociado al token.' });
                res.render('session/resetPassword', { errors: {}, usuario: usuario });
            });
        });
    },

    resetPassword: function (req, res) {
        if (req.body.password != req.body.confirm_password) {
            res.render('session/resetPassword', { errors: { confirm_password: { message: 'No coincide con el password ingresado' } }, usuario: new Usuario({ email: req.body.email }) });
            return;
        }

        Usuario.findOne({ email: req.body.email }, function (err, usuario) {
            usuario.password = req.body.password;
            usuario.save(function (err) {
                if (err) {
                    res.render('session/resetPassword', { errors: err.errors, usuario: new Usuario({ email: req.body.email }) });
                } else {
                    res.redirect('/login');
                }
            });
        });
    }
}