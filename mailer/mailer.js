const nodemailer = require('nodemailer');
const sgTransport = require('nodemailer-sendgrid-transport');

let mailConfig;

if (process.env.NODE_ENV === 'production'){
    console.log("user: "+ process.env.SENDGRID_API_SECRET);
    const options = {
        auth: {
            api_key: process.env.SENDGRID_API_SECRET
        }
    }
    mailConfig = sgTransport(options);
} else {
    if(process.env.NODE_ENV === 'staging'){
        const options = {
            auth: {
                api_key: process.env.SENDGRID_API_SECRET
            }
        }
        mailConfig = sgTransport(options);
    } else{
        console.log("user: "+ process.env.ethereal_user);
        console.log("pass: "+ process.env.ethereal_pwd);
        mailConfig = {
            host: 'smtp.ethereal.email',
            port: 587,
            secure: false,
            auth: {
                user: process.env.ethereal_user,
                pass: process.env.ethereal_pwd
            },
            tls: {
                rejectUnauthorized: false
            }
        };
    }
}



module.exports = nodemailer.createTransport(mailConfig);