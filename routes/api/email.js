var express = require('express');
var router = express.Router();
var emailController = require('../../controllers/api/emailControllerAPI');

router.post('/sendMail', emailController.sendEmail);
module.exports = router;