var express = require('express');
var router = express.Router();
var loginController = require('../controllers/login');

/* GET users listing. */
router.get('/',loginController.login_get);
router.post('/',loginController.login);

module.exports = router;