var express = require('express');
var router = express.Router();
const sessionController = require('../controllers/session');

router.get('/forgotPassword', sessionController.forgotPassword_get);
router.post('/forgotPassword', sessionController.forgotPassword);
router.get('/resetPassword/:token', sessionController.resetPassword_get);
router.post('/resetPassword', sessionController.resetPassword);

module.exports = router;