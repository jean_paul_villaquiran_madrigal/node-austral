var mongoose = require('mongoose');
var Bicicleta = require('../../../models/bicicleta');
var request = require('request');
var server = require('../../../bin/www');

var base_url = "http://localhost:3000/api/bicicletas";

describe("Bicicleta Api", () => {

    beforeEach(function (done) {
        var mongoDB = 'mongodb://localhost:27017/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true })
        .then(() => {
            console.log('La conexion a la base de datos se ha realziado satisfactoriamente!');
        });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'Connection error'));
        db.once('open', function () {
            console.log('[API] We are connected to test database');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if(err) console.log(err);
            done();
        });
    });

    describe('GET BICICLETAS /', () => {
        it('Status 200', (done) => {
            request.get(base_url, function (error, response, body) {
                if(error) console.error(error);
                var resul = JSON.parse(body);
                console.log("resul: "+resul.bicicletas);
                expect(response.statusCode).toBe(200);
                expect(resul.bicicletas.length).toBe(0);
                done();
            });
        });
    });

    describe('POST BICICLETAS /create', () => {
        it('Status 200', (done) => {
            var headers = { 'content-type': 'application/json' };
            var aBici = '{"color":"rojo", "modelo":"urbana","lat":"-34","lng":"-54"}';

            request.post({
                headers: headers,
                url: base_url + '/create',
                body: aBici
            }, function (error, response, body) {
                expect(response.statusCode).toBe(200);
                var aBici = JSON.parse(body).bicicleta;
                expect(aBici.color).toBe("rojo");
                expect(aBici.ubicacion[0]).toBe(-34);
                expect(aBici.ubicacion[1]).toBe(-54);
                done();
            });
        });
    });

    describe('POST BICICLETAS /delete', () => {
        it('Status 200', (done) => {
            var headers = { 'content-type': 'application/json' };
            var aBici = Bicicleta.createInstance(1,"rojo","urbana",[-34,-54]);
            Bicicleta.agregar(aBici);
            
            request.get({
                headers: headers,
                url: base_url
            }, function (error, response, body) {
                var resul = JSON.parse(body);
                console.log("resul: "+resul.bicicletas);
                expect(response.statusCode).toBe(200);
                expect(resul.bicicletas.length).toBe(1);
                done();
            });

        });
    });
});