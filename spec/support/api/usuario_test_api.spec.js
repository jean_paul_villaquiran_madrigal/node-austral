var mongoose = require('mongoose');
var Usuario = require('../../../models/usuario');
var Reserva = require('../../../models/reserva');
var Bicicleta = require('../../../models/bicicleta');

describe("Bicicleta Api", () => {

    beforeEach(function (done) {
        var mongoDB = 'mongodb://localhost:27017/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true })
        .then(() => {
            console.log('La conexion a la base de datos se ha realziado satisfactoriamente!');
        });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'Connection error'));
        db.once('open', function () {
            console.log('[API] We are connected to test database');
            done();
        });
    });

    afterEach(function(done){
        Reserva.deleteMany({}, function(err, success){
            if(err) console.log(err);
            
            Usuario.deleteMany({}, function(err, success){
                if(err) console.log(err);

                Bicicleta.deleteMany({}, function(err, success){
                    if(err) console.log(err);
                    done();
                });
            });
        });
    });

    describe('Cuando un usuario reserva una bicicleta /', () => {
        it('debe existir la reserva', (done) => {
            const usuario = new Usuario({ nombre: "Ezequiel" });
            usuario.save();
            const bicicleta = new Bicicleta({code:1, color: "verdeamarillento", modelo: "urbana", ubicacion: [43,3]});
            bicicleta.save();

            var hoy = new Date();
            var maniana = new Date();
            maniana.setDate(hoy.getDate() + 1);
            usuario.reservar(bicicleta.id, hoy, maniana, function(err, reserva){
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reservas){
                    console.log(reservas[0]);
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasDeReserva()).toBe(2);
                    expect(reservas[0].bicicleta.code).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                });
            });
        });
    });
});