var mongoose = require('mongoose');
const Bicicleta = require('../../../models/bicicleta');

describe('Testing Bicicletas',function(){
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost:27017/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true })
        .then(() => {
            console.log('La conexion a la base de datos se ha realziado satisfactoriamente!');
        });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'MongoDB connection error: '));
        db.once('open', function(){
            console.log('We are connected to test database');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if(err) console.log(err);
            done();
        });
    });

    
    describe('Bicicleta.createInstance',() => {
        it('crea una instancia de bicicleta',() => {
            var bici = Bicicleta.createInstance(1,"verde","urbana",[-34.5,-54.1]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toBe(-34.5);
            expect(bici.ubicacion[1]).toBe(-54.1);
        });
    });
    
    describe('Bicicleta.allBicis',() => {
        it('comienza vacia',(done) => {
            Bicicleta.allBicis(function(err, bicis){
               expect(bicis.length).toBe(0);
               done();
            });
        });
    });

    
    describe('Bicicleta.add',() => {
        it('agrego solo una bicicleta',(done) => {
            var aBici = new Bicicleta({code: 1, color: "verde", modelo:"urbana"});
            Bicicleta.agregar(aBici,function(err, newBici){
               if(err) console.log(err);               
               Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(1);
                expect(bicis[0].code).toEqual(aBici.code);
                done();
               })
            });
        });
    });

    describe('Bicicleta.add',() => {
        it('retorna con code',(done) => {
            
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code: 1, color: "verde", modelo:"urbana"});
                Bicicleta.agregar(aBici,function(err, newBici){
                    if(err) console.log(err);   
                              
                    var aBici2 = new Bicicleta({code: 2, color: "roja", modelo:"urbana"});
                    Bicicleta.agregar(aBici2,function(err, newBici){
                        if(err) console.log(err);               
                        Bicicleta.findByCode(2,function(err, targetBici){
                         expect(targetBici.code).toBe(aBici2.code);
                         expect(targetBici.color).toBe(aBici2.color);
                         expect(targetBici.modelo).toBe(aBici2.modelo);

                         done();
                        })
                     });
                });
            });
        });
    });

});